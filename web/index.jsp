<%-- 
    Document   : index
    Created on : Nov 29, 2022, 6:40:08 PM
    Author     : vagrant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Web App Demo</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p>Today is <%=Calendar.getInstance().getTime()%>
        </p>
        <%
        String greeting;
        int hourOfDay =
        Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (hourOfDay < 12) {
        greeting = "Good morning";
        } else if (hourOfDay >= 12 && hourOfDay < 19) {
        greeting = "Good afternoon";
        } else {
        greeting = "Good evening";
        }
        %>
        <p><%=greeting%></p>
    </body>
</html>